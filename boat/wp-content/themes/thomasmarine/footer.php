</main>
<footer>
  <ul class="wrapper">
    <li>
    <?php
      $args1 = array(
        'theme_location'  => 'FooterMenuFirst',
        'container'       => false,
        'menu_class'      => 'footer-menu'
      );
    wp_nav_menu($args1); ?>
    <ul class="footer-social-menu">
      <li>
        <figure>
          <a href="#" title="Facebook"><img class="social-icon" src="<?php echo get_theme_file_uri('/assets/images/white-facebook-letter-logo.png'); ?>" alt=""></a>
        </figure>
      </li>
      <li>
        <figure>
          <a href="#" title="Instagram"><img class="social-icon" src="<?php echo get_theme_file_uri('/assets/images/white-instagram-logo.png'); ?>" alt=""></a>
        </figure>
      </li>
    </ul>

    <li>  
      <?php $args2 = array(
          'theme_location'  => 'FooterMenuSecond',
          'container'       => false,
          'menu_class'      => 'footer-menu'
        );
      wp_nav_menu($args2); ?>
    </li>

    <li>
      <?php $args3 = array(
          'theme_location'  => 'FooterMenuThird',
          'container'       => false,
          'menu_class'      => 'footer-menu'
        );
      wp_nav_menu($args3); ?>
    </li>

    <li>
      <div class="message-box">
        <span class="message-box-heading"><?php echo get_field('message_box_heading', 'theme_settings_1') ?></span>
        <span><?php echo get_field('message_box_sub_heading', 'theme_settings_1') ?></span>
        <input type="email" name="email" placeholder="Your Email">
        <figure>
          <img src="<?php echo get_field('message_box_email_icon', 'theme_settings_1'); ?>" alt="email icon">
        </figure>
      </div>
    </li>
  </ul>
</footer>

<?php wp_footer(); ?>
</body>
</html>