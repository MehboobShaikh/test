<?php

function thomasmarine_resources(){
	wp_enqueue_script('jquery');
	wp_enqueue_style('slider-css1','//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css');
	wp_enqueue_style('slider-css2','//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css');
	wp_enqueue_script('slider-jquery','//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js',NULL,1.0,true);
	wp_enqueue_script('slider','//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js','slider-jquery',1.0,true);
	wp_enqueue_script('slider-script',get_theme_file_uri('/assets/vendor/js/slider.js'),'slider',1.0,true);
	wp_enqueue_style('my-slider-style',get_theme_file_uri('/assets/vendor/css/slider.css'));
	wp_enqueue_style('my-style',get_stylesheet_uri());
}
add_action('wp_enqueue_scripts','thomasmarine_resources');

// thomasmarine
function thomasmarine_customize_site_logo($wp_customize){
	$wp_customize->add_setting('thomasmarine_site_header_logo_setting',array(
		'default'	=>	get_theme_file_uri().'/assets/images/thomas-marine-logo.png'
	));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'thomasmarine_site_header_logo_setting',array(
		'label'			=>	__('Site Header Logo','thomasmarine'),
		'section'		=>	'title_tagline',
		'setting'		=>	'thomasmarine_site_header_logo_setting'
	)));

}
add_action('customize_register','thomasmarine_customize_site_logo');



function thomasmarine_features() {
  add_theme_support('title-tag');
  	// Post Thumbnails
	add_theme_support('post-thumbnails');
	add_image_size('small', 120, '', true);
}
add_action('after_setup_theme', 'thomasmarine_features');



function thomasmarine_register_new_menu(){
	register_nav_menu('HeaderMenu',__('Header Menu','thomasmarine'));
	register_nav_menu('FooterMenuFirst',__('Footer Menu First','thomasmarine'));
	register_nav_menu('FooterMenuSecond',__('Footer Menu Second','thomasmarine'));
	register_nav_menu('FooterMenuThird',__('Footer Menu Third','thomasmarine'));
}
add_action('init','thomasmarine_register_new_menu');


function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;

  // Hide the editor on the Contact Page
  $page_title = get_the_title($post_id);
  if($page_title == 'Contact Us'){ 
    remove_post_type_support('page', 'editor');
  }
}
add_action( 'admin_init', 'hide_editor' );


function thomasmarine_register_post_types() {

	/**
	 * Post Type: Boat.
	 */

	$labels = array(
		"name" => __( "Boats", "thomasmarine" ),
		"all_items" => __( "All Boats", "thomasmarine" ),
		// "new_item" => __( "New Boat", "thomasmarine" ),
		"add_new" => __( "Add New Boat", "thomasmarine" ),
		"singular_name" => __( "Boat", "thomasmarine" ),
		"plural_name" => __( "Boats", "thomasmarine" ),
		"archives" => __( "Boats Archive", "thomasmarine" ),
	);

	$args = array(
		"label" => __( "Boat", "thomasmarine" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"hierarchical" => false,
		"rewrite" => array( "slug" => "boat", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "thumbnail" ),
	);

	register_post_type( "boat", $args );
}

add_action( 'init', 'thomasmarine_register_post_types' );


function thomasmarine_create_custom_taxonomy(){
	//For Boat Boat Type
	register_taxonomy(
		'boat-type',
		'boat',
		array(
			'label' => __('Boat Types','thomasmarine'),
			'rewrite' => array( 'slug' => 'boat-type' ),
            'hierarchical' => true,
            'show_admin_column' => true,
		)
	);

	//For Boat Makes
	register_taxonomy(
		'makes',
		'boat',
		array(
			'label' => __('Makes','thomasmarine'),
			'rewrite' => array( 'slug' => 'makes' ),
            'hierarchical' => true,
            'show_admin_column' => true,
		)
	);
}

add_action('init','thomasmarine_create_custom_taxonomy');


// ===================================== AJAX GOES HERE FOR Filteration ============================= 


function filter_boats(){
	wp_enqueue_script('filter-script',get_template_directory_uri().'/assets/js/filter-function.js','jquery',1.0,true);
	wp_localize_script('filter-script', 'ajaxUrlData', array(
    'url' => admin_url('admin-ajax.php')
  ));
}
add_action('filter_my_boats','filter_boats');

//AJAX ACTION on FILTER

// add_action('wp_ajax_filter_ajax_action','filter_post_ajax_action');
// add_action('wp_ajax_nopriv_filter_ajax_action','filter_post_ajax_action');

function filter_post_ajax_action(){
	require 'filter-content.php';
}


// ======================= OPTION PAGE ==================

function thomasmarine_option_page(){
	if( function_exists('acf_add_options_page') ) {
		// OPTION PAGE THEME SETTING
		acf_add_options_page(array(
			'page_title' 	=> 'ThomasMarine Settings Page',
			'menu_title'	=> 'ThomasMarine Settings',
			'menu_slug' 	=> 'thomasmarine-settings',
			'post_id'		=> 'theme_settings_1',
			'update_button'		=> __('Save Options', 'acf'),
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}
}

add_action('init','thomasmarine_option_page');

// ============================ ADD Active Class to Menu ===========================

function thomasmarine_special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'menu-item-active ';
     }
     return $classes;
}
add_filter('nav_menu_css_class' , 'thomasmarine_special_nav_class' , 10 , 2);

