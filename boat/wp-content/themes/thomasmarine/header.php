<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header>
		<div class="wrapper">
			<?php if(get_theme_mod('thomasmarine_site_header_logo_setting')) { ?>
				<h1><a href="<?php site_url() ?>" title="<?php bloginfo('title') ?> Logo"><img src="<?php echo get_theme_mod('thomasmarine_site_header_logo_setting'); ?>" alt="<?php bloginfo('title') ?> Logo"></a></h1>
			<?php } else { ?>
				<h1><a href="<?php site_url() ?>" title="<?php bloginfo('title') ?> Logo"><?php bloginfo('title'); ?></a></h1>
			<?php } ?>
			<nav class="primary-nav cf">
				<ul class="social-menu">
					<li><figure><a href="#" title="Facebook"><img class="social-icon" src="<?php echo get_theme_file_uri('/assets/images/black-facebook-letter-logo.png'); ?>" alt=""></a></figure></li>
					<li><figure><a href="#" title="instagram"><img class="social-icon" src="<?php echo get_theme_file_uri('/assets/images/black-instagram-logo.png'); ?>" alt=""></a></figure></li>
				</ul>

				<?php
					$args = array(
						'theme_location' 	=> 'HeaderMenu',
						'container'				=> false,
						'menu_class'			=> 'menu-pages',
					);
				?>
				<?php wp_nav_menu($args); ?>
			</nav>
		</div>
	</header>
	<main class="wrapper">

