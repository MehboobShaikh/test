<?php get_header(); ?>

<?php
	if(have_posts()){
		while(have_posts()){ the_post();
		$heading = get_field('banner_heading');
		$subheading = get_field('banner_sub_heading');
		$bannerSearchImage = get_field('banner_search_image');
		// var_dump(get_field('banner_search_image'));
		$bannerImage = get_field('banner_image'); ?>
	<div class="banner">

		<h2 class="banner__heading"><?php echo $heading; ?></h2>
		<span class="banner__sub-heading"><?php echo $subheading; ?></span>
		<figure class="banner__search-img"><img src="<?php echo $bannerSearchImage; ?>" alt=""></figure>	
	</div>

	<section class="search-section">
		<h4><?php echo get_field('page_heading'); ?></h4>
		<div class="selection">
			<select class="select select-condition">
				<option>Select Condition</option>
				<option>New</option>
				<option>Pre Owned</option>
			</select>
			
			<?php
				$makes = get_terms( array(
					'taxonomy' => 'makes',
			    'orderby' => 'taxonomy'
			  ));
			  $i = 0;
			  $counts = count($makes);
			  // var_dump($makes);
			?>
			<select class="select select-makes">
				<option>Select Makes</option>
				<?php
					while($i < $counts){
						echo "<option>" . $makes[$i]->name . "</option>";
						$i++;
					}
				?> 
			</select>

			<?php
				$boattype = get_terms( array(
					'taxonomy' => 'boat-type',
			    'orderby' => 'taxonomy'
			  ));
			  $i = 0;
			  $counts = count($boattype);
			  // var_dump($boattype);
			?>

			<select class="select select-type">
				<option>Selectc Type</option>
				 <?php
					while($i < $counts){
						echo "<option>" . $boattype[$i]->name . "</option>";
						$i++;
					}
				?> 
			</select>
			<button>Search</button>
			<?php do_action('filter_my_boats'); ?>
		</div>
		<div id="output">
			
		</div>
	</section>
	<section class="wrapper featured">
	
	<h5><a href="<?php echo get_post_type_archive_link( 'boat' ); ?>" title="Featured">Featured</a></h5>
	<div class="main">
	  <div class="slider slider-for">

			<?php
				$boat_query = new WP_Query(array(
					'post_type'			=> 'boat',
					'posts_per_page'	=>	-1,
				));

				if($boat_query->have_posts()){
					// echo "<div class='feature-wrapper'>";
					while($boat_query->have_posts()){
						$boat_query->the_post();
						$imgurl = get_the_post_thumbnail_url();
						// var_dump($imgurl);
						echo '<figure><img class="feature-img" src="' . $imgurl . '">';
						echo '<figcaption>' . the_title() . '</figcaption>';
					echo "</figure>";
					} wp_reset_postdata();
				}
			?>
		</div>
	</div>

	</section>

	<section class="family-story">
		<h3><?php echo get_field('family_story_heading'); ?></h3>
		<span><?php echo get_field('family_story_sub_heading'); ?></span>
		<iframe src="<?php echo get_field('family_story_url'); ?>" frameborder="0" allowfullscreen></iframe>
	</section>

	<section class="special">
		<h5>Special</h5>
		<?php
			$special_query = new WP_Query(array(
				'post_type'		=> 'boat',
				'posts_per_page'	=> -1,
				'meta_query'    => array(
			    array(
			        'key'       => 'promotion',
			        'value'     => 'yes',
			        'compare'   => 'LIKE',
			      )
			  )
			));

			if($special_query->have_posts()){ ?>
					<ul class="promotion-boats">
						<?php while($special_query->have_posts()){
							$special_query->the_post(); ?>

								<li>
									<figure><img class="promotion-images" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="featured images"></figure>
									<div class="promotion-prize">
										<span class="new-prize">$<?php echo get_field('promotion_prize'); ?></span><span class="old-prize"><strike>$<?php echo get_field('boat_price'); ?></strike></span>
										<span class="boat-info"><?php the_title(); ?></span>
											<?php if(get_field('boat_length')){ ?>
												<span class="boat-info">length <?php echo get_field('boat_length'); ?></span>
											<?php } ?>
									</div>
								</li>

						<?php } wp_reset_postdata(); ?>
					</ul>
			<?php }
		?>
	</section>

	<div class="clients">
		<div class="slider client-slider">

			<?php
				if( have_rows('client_details') ){
					while(have_rows('client_details')){
						the_row(); ?>

						<figure>
							<img src="<?php echo get_sub_field('client_company_logo'); ?>" alt="">
						</figure>
						
					<?php }
				}
			?>

		</div>
	</div>

	<div class="about-yor-boat">
		<div class="about-yor-boat-info">
			<h3><?php echo get_field('about_your_boat_banner_heading'); ?></h3>
			<p><?php echo get_field('about_your_boat_banner_description'); ?></p>
			<a class="write-us-btn" href="#" title=""><?php if(get_field('about_your_boat_banner_button_text')) {
				echo get_field('about_your_boat_banner_button_text');
			}else{ echo "Write Us"; } ?></a>
		</div>
	</div>

	<?php }
	}
?>

<?php get_footer(); ?>